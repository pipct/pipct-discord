import asyncio
import queue
import threading


class AudioPlayer(object):
    def __init__(self, client):
        self.client = client
        self.play_queue = queue.Queue()
        self.playing = False

    def end_audio(self, voice):
        if not self.play_queue.empty():
            next = self.play_queue.get()
            if voice is not None and next[0] == voice.channel:
                coro = self._play_audio(next[0], next[1], voice)
                fut = asyncio.run_coroutine_threadsafe(coro, self.client.loop)
            else:
                if voice is not None:
                    self.voice_disconnect(voice)
                coro = self.client.join_voice_channel(next[0])
                fut = asyncio.run_coroutine_threadsafe(coro, self.client.loop)
                voice = fut.result()
                acoro = self._play_audio(next[0], next[1], voice)
                afut = asyncio.run_coroutine_threadsafe(acoro, self.client.loop)
                afut.result()
        else:
            self.voice_disconnect(voice)
            self.playing = False

    def voice_disconnect(self, voice):
        coro = voice.disconnect()
        fut = asyncio.run_coroutine_threadsafe(coro, self.client.loop)
        try:
            fut.result()
        except:
            # an error happened sending the message
            pass

    async def play_audio(self, channel, file):
        if not self.playing:
            self.playing = True
            self.play_queue.put((channel, file))
            threading.Thread(target=lambda: self.end_audio(None)).start()
        else:
            self.play_queue.put((channel, file))

    async def _play_audio(self, channel, file, voice):
        player = voice.create_ffmpeg_player(file, after=lambda: self.end_audio(voice))
        player.start()
