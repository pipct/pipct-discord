import glob
from os.path import basename


class AudioLibrary(object):

    def __init__(self):
        self.audiolist = {}
        self.reload_audio()

    def reload_audio(self):
        for file in glob.glob('audio/*'):
            name = basename(file).rsplit('.', 1)[0]
            self.audiolist[name] = file
