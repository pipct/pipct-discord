# pipct-discord [![Build Status][ci-image]][ci-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coverage-image]][coverage-url]
> it's a discord bot yay

![Logo](logo.png)

## Installation

Clone this repository, and run:
```sh
$ npm install
```

## Usage

```js
$ npm run bot
```
## License

 © Rhys Davies and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

[ci-image]: https://gitlab.com/pipct/pipct-discord/badges/master/build.svg
[ci-url]: https://gitlab.com/pipct/pipct-discord/commits/master
[daviddm-image]: https://david-dm.org/pipct/pipct-discord.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/pipct/pipct-discord
[coverage-image]: https://gitlab.com/pipct/pipct-discord/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/pipct/pipct-discord/commits/master
