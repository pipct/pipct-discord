#!/usr/bin/env python3

import discord
import logging
import configparser
import signal
import sys
from audio_library import AudioLibrary
from audio_player import AudioPlayer

config = configparser.ConfigParser()
config.read("config.cfg")

logging.basicConfig(level=logging.ERROR)

client = discord.Client()
audio = AudioLibrary()
audio_player = AudioPlayer(client)

@client.event
async def on_ready():
    await client.change_presence(game=discord.Game(name=config['General'].get('status', 'Python!')))


@client.event
async def on_message(message):
    if not message.content.startswith('!'):
        return
    if message.content == '!audioreload':
        audio.reload_audio()
        print(audio.audiolist)
    if message.content[1:] in audio.audiolist:
        await audio_player.play_audio(message.author.voice.voice_channel, audio.audiolist[message.content[1:]])
        await client.delete_message(message)


@client.event
async def on_voice_state_update(before, after):
    if after.bot:
        return
    if after.voice.voice_channel is not None and before.voice.voice_channel != after.voice.voice_channel:
        await client.send_message(client.get_channel(config['LoginNotify']['notifyChannelID']), after.display_name + ' has joined ' + after.voice.voice_channel.name)
        sound = config['UserJoinSounds'].get(after.id, config['UserJoinSounds']['default'])
        await audio_player.play_audio(after.voice.voice_channel, audio.audiolist[sound])


def exit_handler(signal, frame):
    sys.exit(0)


signal.signal(signal.SIGINT, exit_handler)
signal.signal(signal.SIGTERM, exit_handler)
client.run(config['General']['token'])
